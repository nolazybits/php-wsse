<?php
namespace Zeflasher\Wsse;
/**
 * User: zeflasher
 * Date: 25/06/12
 * Time: 8:07 PM
 */
class WsseConstants
{
    const WSSE_HTTP_METHOD_GET             = 'GET';
    const WSSE_HTTP_METHOD_POST            = 'POST';
    const WSSE_HTTP_METHOD_PUT             = 'PUT';
    const WSSE_HTTP_METHOD_HEAD            = 'HEAD';
    const WSSE_HTTP_METHOD_DELETE          = 'DELETE';
}
