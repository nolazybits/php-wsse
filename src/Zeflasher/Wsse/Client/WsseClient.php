<?php
namespace Zeflasher\Wsse\Client;

/**
 * Created by IntelliJ IDEA.
 * User: zeflasher
 * Date: 7/03/13
 * Time: 10:49 AM
 * To change this template use File | Settings | File Templates.
 */ 

class WsseClient extends \Zeflasher\Wsse\Provider\WsseRequest
{
    function __construct(  $username= null, $password = null )
    {
        $this->_username = $username;
        $this->_password = $password;
    }

    protected function _toHeader()
    {
        return 'UsernameToken Username="' . $this->_username . '", PasswordDigest="' . $this->_password_digest . '", Nonce="' . $this->_nonce . '", Created="' . $created .'"';
    }

    public function createWsseParameters()
    {
        $nonce = \Zeflasher\Wsse\WsseUtil::generate_nonce();
        $dateTime = new \DateTime();
        $created = $dateTime->format(\DateTime::ATOM);

        $this->_wsseParameters = array
        (
            "Username"              => $this->_username,
            "PasswordDigest"        => base64_encode(sha1(utf8_encode(base64_decode($nonce)). $created. utf8_encode($this->_password), true)),
            "Nonce"                 => $nonce,
            "Created"               => $created
        );

//        $this->_wsseParameters = array_merge($wsseParameters, $this->_parameters);
    }

    /**
     * @param string $url All the query string will be removed. Passed them in the param array
     * @param string $method GET, POST, PUT, DELETE, and any custom ones you would provide
     * @param array $parameters Array in the following format ['key' => value]
     * @param string $returnType
     * @param string $proxy
     * @return array
     */
    public function request($url, $method = 'GET', Array $parameters = [], $returnType = 'json', $proxy = null)
    {
        $this->_http_url = $url;
        $this->_http_method = $method;
        $this->_parameters = $parameters;

    //  add the Wsse parameters
        $this->createWsseParameters();

    //  create the curl options
        $curl_options = array
        (
            CURLOPT_URL => \Zeflasher\Wsse\WsseUtil::get_normalized_http_url($this->_http_url),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        );

        if( $proxy )
        {
            $curl_options[CURLOPT_PROXY] = $proxy;
        }

        switch($method)
        {
            //  get
            case \Zeflasher\Wsse\WsseConstants::WSSE_HTTP_METHOD_GET:
                $curl_options[CURLOPT_HTTPGET] = 1;
                $curl_options[CURLOPT_URL] = $this->to_url();
            break;

            //  set the post parameters if post method
            case \Zeflasher\Wsse\WsseConstants::WSSE_HTTP_METHOD_POST:
                $curl_options[CURLOPT_POST] = 1;
                $curl_options[CURLOPT_POSTFIELDS] = $this->get_query_parameters();
            break;

            //  set the put parameters if put method
            case \Zeflasher\Wsse\WsseConstants::WSSE_HTTP_METHOD_PUT:
                $curl_options[CURLOPT_CUSTOMREQUEST] = 'PUT';
                $curl_options[CURLOPT_POSTFIELDS] = $this->get_query_parameters();
            break;

            //  set the put parameters if put method
            case \Zeflasher\Wsse\WsseConstants::WSSE_HTTP_METHOD_DELETE:
                $curl_options[CURLOPT_CUSTOMREQUEST] = 'DELETE';
                $curl_options[CURLOPT_POSTFIELDS] = $this->get_query_parameters();
            break;
        }

        //  Add the headers
        $curl_options[CURLOPT_HTTPHEADER] = $this->to_header();
        $curl_options[CURLOPT_HEADER] = true;
        $curl_options[CURLOPT_FOLLOWLOCATION] = true;


        // make CURL request
        $curl = curl_init();
        curl_setopt_array($curl, $curl_options);
        $response = curl_exec($curl);
        $error = curl_error($curl);
        $result = new \stdClass();

        if ( $error != "" )
        {
            $result->curl_error = $error;
            return $result;
        }

        $header_size = curl_getinfo($curl,CURLINFO_HEADER_SIZE);
        $result->header     = substr($response, 0, $header_size);
        $result->body       = substr( $response, $header_size );
        $result->http_code  = curl_getinfo($curl,CURLINFO_HTTP_CODE);
        $result->last_url   = curl_getinfo($curl,CURLINFO_EFFECTIVE_URL);

        curl_close($curl);
        return $result;
    }

    /**
     * The body request parameters (excluded oauth), sorted and concatenated into a normalized string.
     * @return string
     */
    public function get_query_parameters()
    {
        return \Zeflasher\Wsse\WsseUtil::build_http_query($this->_parameters);
    }
}
